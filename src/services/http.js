/*
 * @Author: Patrick-Jun
 * @Date: 2021-07-10 14:04:07
 * @Last Modified by: Patrick-Jun
 * @Last Modified time: 2021-11-30 18:34:14
 */

import Toast from '@vant/weapp/toast/toast';

/**
 * get请求
 * @param {string} url 请求地址
 * @param {object} params 请求参数
 * @param {boolean} showloading 是否显示加载中
 */
const get = (url, params, showloading) => {
  // 请求头
  const header = {
    'content-type': 'application/json',
  };
  if (showloading) {
    wx.showLoading({
      title: '加载中...',
      mask: false,
    });
  }
  return new Promise((resolved, rejected) => {
    wx.request({
      url,
      data: params,
      header,
      method: 'GET',
      dataType: 'json',
      responseType: 'text',
      success: (res) => {
        if (res.data.isSuccess) {
          resolved(res.data.data);
        } else {
          rejected({
            msg: '服务器故障',
            code: 200,
          });
        }
      },
      fail: (res) => {
        Toast.fail('网络开小差啦');
        rejected({
          msg: '网络开小差啦',
          code: res.statusCode,
        });
      },
      complete: () => {
        if (showloading) {
          wx.hideLoading();
        }
        //停止刷新
        wx.stopPullDownRefresh();
        // 隐藏顶部刷新图标
        wx.hideNavigationBarLoading();
      },
    });
  });
};

/**
 * post请求
 * @param {string} url 请求地址
 * @param {object} params 请求参数
 * @param {boolean} showloading 是否显示加载中
 */
const post = (url, params, showloading) => {
  // 请求头
  const header = {
    'content-type': 'application/json',
  };
  if (showloading) {
    wx.showLoading({
      title: '加载中...',
      mask: false,
    });
  }
  return new Promise((resolved, rejected) => {
    wx.request({
      url,
      data: params,
      header,
      method: 'POST',
      dataType: 'json',
      responseType: 'text',
      success: (res) => {
        if (res.data.isSuccess) {
          resolved(res.data.data);
        } else {
          rejected({
            msg: '服务器故障',
            code: 200,
          });
        }
      },
      fail: (res) => {
        Toast.fail('网络开小差啦');
        rejected({
          msg: '网络开小差啦',
          code: res.statusCode,
        });
      },
      complete: () => {
        if (showloading) {
          wx.hideLoading();
        }
        //停止刷新
        wx.stopPullDownRefresh();
        // 隐藏顶部刷新图标
        wx.hideNavigationBarLoading();
      },
    });
  });
};

// 文件
const file = {
  /**
   * 上传
   * @param {string} url 请求地址
   * @param {string} path 文件路径
   * @param {boolean} showloading 是否显示上传中
   */
  upload: (url, path, showloading) => new Promise((resolved, rejected) => {
    if (showloading) {
      wx.showLoading({
        title: '上传中...',
        mask: false,
      });
    }
    wx.uploadFile({
      url,
      filePath: path,
      name: 'img',
      success: (res) => {
        const data = JSON.parse(res.data);
        if (data.isSuccess) {
          resolved(data.data);
        } else {
          rejected({
            msg: '服务器故障',
            code: 200,
          });
        }
      },
      fail: (res) => {
        Toast.fail('网络开小差啦');
        rejected({
          msg: '网络开小差啦',
          code: res.statusCode,
        });
      },
      complete: () => {
        if (showloading) {
          wx.hideLoading();
        }
      },
    });
  }),
};

module.exports = {
  get,
  post,
  file,
};
