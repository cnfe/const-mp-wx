/*
 * @Author: Patrick-Jun
 * @Date: 2021-05-28 14:27:34
 * @Last Modified by: Patrick-Jun
 * @Last Modified time: 2021-05-30 19:33:06
 */

App({
  onLaunch() {
    // 获取手机系统信息
    this.globalData.deviceInfo = wx.getSystemInfoSync();

    // wx.redirectTo({
    //   url: 'pages/guide/guide',
    // });
  },
  globalData: {
    userInfo: null,
    deviceInfo: null,
  },
});
