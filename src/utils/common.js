/*
 * @Author: Patrick-Jun
 * @Date: 2021-05-28 14:27:47
 * @Last Modified by: Patrick-Jun
 * @Last Modified time: 2021-11-30 18:42:39
 */

/**
 * 常用交互方法
 */

/**
 * 结束下拉刷新：当使用了下拉刷新，请求结束时调用
 */
 export const stopRefresh = () => {
  //停止刷新
  wx.stopPullDownRefresh();
  // 隐藏顶部刷新图标
  wx.hideNavigationBarLoading();
};
