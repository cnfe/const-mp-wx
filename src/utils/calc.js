/* eslint-disable no-param-reassign */
/*
 * @Author: Patrick-Jun
 * @Date: 2021-06-21 20:11:24
 * @Last Modified by: Patrick-Jun
 * @Last Modified time: 2021-11-30 18:41:22
 */

import CryptoJS from 'crypto-js';
import { Base64 } from 'js-base64';

/**
 * 常用计算方法实现
 */

/**
 * 微信加解密
 */
export const wxDataDecrypt = (appid, sessionKey, encryptedData, ivv) => {
  const key = CryptoJS.enc.Base64.parse(sessionKey);
  const iv = CryptoJS.enc.Base64.parse(ivv);
  const decrypt = CryptoJS.AES.decrypt(encryptedData, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7
  });
  return JSON.parse(Base64.decode(CryptoJS.enc.Base64.stringify(decrypt)));
};

/**
 * Parse the time to string
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string}
 */
export const parseTime = (time, cFormat, fillZero = true) => {
  if (arguments.length === 0) {
    return null;
  }
  if (!time) {
    return null;
  }
  let format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}';
  let date;
  if (typeof time === 'object') {
    date = time;
  } else {
    if (typeof time === 'string' && /^[0-9]+$/.test(time)) {
      time = parseInt(time);
    }
    if (typeof time === 'number' && time.toString().length === 10) {
      time = time * 1000;
    }
    date = new Date(time);
  }
  let formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  };
  return format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = formatObj[key];
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') {
        return ['日', '一', '二', '三', '四', '五', '六'][value];
    }
    if (fillZero) {
        if (result.length > 0 && value < 10) {
            value = '0' + value;
        }
    }
    return value || 0;
  });
}

/**
 * @description 获取当前时间2020-00-00 00:00:00
 * @param split 分隔符
 * @returns {string} 2020-00-00 00:00:00
 */
export const getTodayTime = split => {
  if (!split) {
    split = '/';
  }
  const dt = new Date();
  const year = dt.getFullYear();
  let month = Number(dt.getMonth()) + 1;
  let day = dt.getDate();
  let hours = dt.getHours();
  let minutes = dt.getMinutes();
  let seconds = dt.getSeconds();

  month = month < 10 ? `0${month}` : month;
  day = day < 10 ? `0${day}` : day;
  hours = hours < 10 ? `0${hours}` : hours;
  minutes = minutes < 10 ? `0${minutes}` : minutes;
  seconds = seconds < 10 ? `0${seconds}` : seconds;

  return `${year}${split}${month}${split}${day} ${hours}:${minutes}:${seconds}`;
};

/**
 * @description 获取今天2020-00-00
 * @param split 分隔符
 * @returns {string} 2020-00-00
 */
export const getToday = split => getTodayTime(split).substr(0, 10);

/**
 * @description: 计算a、b两点经纬度的距离
 * @params a:{lon, lat},  b:{lon, lat}
 * @return m
 */
export const caculateDistance = (pointA, pointB) => {
  console.log(pointA, pointB)
  if (pointA.lon === pointB.lon && pointA.lat === pointB.lat) {
    return 0;
  }
  const EARTH_RADIUS = 6378138.0; //单位M
  const PI = Math.PI;

  const getRad = d => d * PI / 180;
  const R = EARTH_RADIUS; // Radius of the earth in km
  const dLat = getRad(pointB.lat-pointA.lat);  // getRad below
  const dLon = getRad(pointB.lon-pointA.lon);
  const a =
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(getRad(pointA.lat)) * Math.cos(getRad(pointB.lat)) *
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  const d = R * c; // Distance in m
  return d;
}

/**
 * @description 脱敏手机号
 * @param phone 手机号
 * @returns {string} 155****0000
 */
export const delPhone = (phone) => {
  if (!phone) {
    return '';
  }
  return `${phone.slice(0, 3)}****${phone.slice(-4)}`
}
